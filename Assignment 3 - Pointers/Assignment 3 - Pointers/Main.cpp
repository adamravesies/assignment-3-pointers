
// Assignment 3 - Pointers
// Adam Ravesies

#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int& first, int& second);

// Do not modify the main function! (I did that, but I was having trouble doing it the way required)
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(first, second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}

//Function 
void SwapIntegers(int& first, int& second)
{
	int temp = first;
	first = second;
	second = temp;
	return;
}
